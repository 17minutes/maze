﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace MazeLib
{
    public class Instance
    {
        public string Forward { get; }
        public string Backward { get; }

        public int Id { get; }

        public Instance(string fwd, string bwd, int id)
        {
            Forward = fwd;
            Backward = bwd;
            Id = id;
        }
        public IEnumerable<(int, int, MoveDirection)> SimulateForward()
        {
            return Simulate(Forward, (0, 0, MoveDirection.South));
        }

        public IEnumerable<(int, int, MoveDirection)> SimulateBackward()
        {
            (int row, int col, MoveDirection dir) = SimulateForward().Last();
            return Simulate(Backward, (row, col, dir.Clockwise().Clockwise()));
        }

        private static IEnumerable<(int, int, MoveDirection)> Simulate(string instructions, (int, int, MoveDirection) initState)
        {
            (int row, int col, MoveDirection dir) = initState;
            foreach (char token in instructions)
            {
                yield return (row, col, dir);
                (row, col, dir) = ExecuteStep(row, col, dir, token);
            }
            yield return (row, col, dir);
        }

        private static (int row, int col, MoveDirection dir) ExecuteStep(int row, int col, MoveDirection dir, char token)
        {
            switch (token)
            {
                case 'W':
                    (int dx, int dy) = dir.GetOffset();
                    return (row + dx, col + dy, dir);
                case 'L':
                    return (row, col, dir.Counterclockwise());
                case 'R':
                    return (row, col, dir.Clockwise());
                default:
                    throw new InvalidOperationException();
            }
        }
    }
}
