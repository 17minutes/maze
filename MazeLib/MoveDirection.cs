﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MazeLib
{
    public enum MoveDirection
    {
        North, East, South, West
    }

    public static class MoveDirectionExtensions
    {
        public static string AsArrow(this MoveDirection d)
        {
            switch (d)
            {
                case MoveDirection.North:
                    return "↑";
                case MoveDirection.South:
                    return "↓";
                case MoveDirection.West:
                    return "←";
                case MoveDirection.East:
                    return "→";
                default:
                    throw new NotSupportedException();
            }
        }

        public static (int, int) GetOffset(this MoveDirection d)
        {
            switch (d)
            {
                case MoveDirection.North:
                    return (-1, 0);
                case MoveDirection.South:
                    return (+1, 0);
                case MoveDirection.West:
                    return (0, -1);
                case MoveDirection.East:
                    return (0, +1);
                default:
                    throw new NotSupportedException();
            }
        }

        public static MoveDirection Clockwise(this MoveDirection d)
        {
            switch (d)
            {
                case MoveDirection.North:
                    return MoveDirection.East;
                case MoveDirection.East:
                    return MoveDirection.South;
                case MoveDirection.South:
                    return MoveDirection.West;
                case MoveDirection.West:
                    return MoveDirection.North;
                default:
                    throw new NotSupportedException();
            }
        }

        public static MoveDirection Counterclockwise(this MoveDirection d)
        {
            switch (d)
            {
                case MoveDirection.North:
                    return MoveDirection.West;
                case MoveDirection.West:
                    return MoveDirection.South;
                case MoveDirection.South:
                    return MoveDirection.East;
                case MoveDirection.East:
                    return MoveDirection.North;
                default:
                    throw new NotSupportedException();
            }
        }
    }
}
