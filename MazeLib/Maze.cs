﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace MazeLib
{
    public class Maze
    {
        private Maze(Room[,] roomMap, Instance instance)
        {
            RoomMap = roomMap;
            BaseInstance = instance;
        }

        public class Room
        {
            public ISet<MoveDirection> OpenDirections { get; }

            public Room()
            {
                OpenDirections = new HashSet<MoveDirection>();
            }

            public void OpenFromDir(MoveDirection dir)
            {
                OpenDirections.Add(dir);
            }

            public override string ToString()
            {
                string choices = "0123456789abcdef";
                int ix = 0;
                if (OpenDirections.Contains(MoveDirection.North)) ix += 1;
                if (OpenDirections.Contains(MoveDirection.South)) ix += 2;
                if (OpenDirections.Contains(MoveDirection.West)) ix += 4;
                if (OpenDirections.Contains(MoveDirection.East)) ix += 8;
                Debug.Assert(ix > 0);
                return choices[ix].ToString();
            }
        }

        public Instance BaseInstance { get; }
        public Room[,] RoomMap { get; }

        public static Maze ConstructMaze(Instance instance)
        {
            var fwd = instance.SimulateForward().Skip(1).SkipLast(1).ToList();
            var bwd = instance.SimulateBackward().Skip(1).SkipLast(1).ToList();
            var full = fwd.Concat(bwd).ToList();

            var rowmin = full.Min(s => s.Item1);
            var colmin = full.Min(s => s.Item2);
            full = full.Select(s => (s.Item1 - rowmin, s.Item2 - colmin, s.Item3)).ToList();

            Room[,] roomMap = new Room[full.Max(s => s.Item1) + 1, full.Max(s => s.Item2) + 1];
            for (int i = 0; i < roomMap.GetLength(0); ++i)
                for (int j = 0; j < roomMap.GetLength(1); ++j)
                    roomMap[i, j] = new Room();
            roomMap[full[0].Item1, full[0].Item2].OpenFromDir(MoveDirection.North);

            for (int i = 0; i < full.Count; ++i)
            {
                var x = full[i];
                if (i != full.Count - 1)
                {
                    var y = full[i + 1];
                    if (x.Item1 == y.Item1 &&
                        x.Item2 == y.Item2 &&
                        x.Item3.Clockwise().Clockwise() != y.Item3)
                    {
                        continue;
                    }
                }
                roomMap[x.Item1, x.Item2].OpenFromDir(x.Item3);
            }

            return new Maze(roomMap, instance);
        }

        public override string ToString()
        {
            var res = $"Case #{BaseInstance.Id}:";
            for (int i = 0; i < RoomMap.GetLength(0); ++i)
            {
                res += "\n";
                for (int j = 0; j < RoomMap.GetLength(1); ++j)
                    res += RoomMap[i, j].ToString();
            }
            return res;
        }
    }
}
