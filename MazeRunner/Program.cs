﻿using MazeLib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace MazeRunner
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 2)
            {
                Console.WriteLine("Usage: MazeRunner.exe <input file> <output file>");
                return;
            }

            string inputFile = args[0];
            string outputFile = args[1];

            using (StreamWriter sw = File.CreateText(outputFile))
            {
                foreach (var maze in ParseInstances(inputFile).Select(Maze.ConstructMaze))
                    sw.WriteLine(maze);
            }
        }

        static IEnumerable<Instance> ParseInstances(string inputFile)
        {
            using (StreamReader sr = File.OpenText(inputFile))
            {
                var nTests = int.Parse(sr.ReadLine());
                for (int test = 1; test <= nTests; ++test)
                {
                    string desc = sr.ReadLine();
                    if (desc == null)
                        throw new EndOfStreamException();
                    else
                    {
                        var descStrings = desc.Split(" ");
                        if (descStrings.Length == 2)
                        {
                            yield return new Instance(descStrings[0], descStrings[1], test);
                        }
                        else throw new FormatException();
                    }
                }
            }
        }
    }
}
